from django.contrib import admin

# Register your models here.

from core import models # from .models import *


@admin.register(models.GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "gg_name",
        "user",
        "timestamp",
        "final_date",
        "days_durations"
    ]


@admin.register(models.Area)
class Area(admin.ModelAdmin):
    list_display = [
        "area_name",
        "user",
        "timestamp"
    ]


@admin.register(models.SubGrantGoal)
class SubGrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "sgg_name",
        "user",
        "timestamp",
        "final_date",
        "days_durations"
    ]


@admin.register(models.Goal)
class GoalAdmin(admin.ModelAdmin):
    list_display = [
        "goal_name",
        "user",
        "timestamp",
        "final_date",
        "days_durations"
    ]


@admin.register(models.Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = [
        "issue_name",
        "user",
        "timestamp",
        "final_date",
        "days_durations"
    ]