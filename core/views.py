from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from django.contrib.auth.mixins import LoginRequiredMixin

from core import models
from core import forms

# Create your views here.


##### GRANT GOAL CRUD #####

### CREATE
class CreateGrantGoal(LoginRequiredMixin, generic.CreateView):
    template_name = "core/create_gg.html"
    model = models.GrantGoal
    form_class = forms.CreateGrantGoalForm
    success_url = reverse_lazy("core:list_gg")
    login_url = "home:index"


### RETRIEVE
# List
class ListGrantGoal(generic.View):
    template_name = "core/list_gg.html"
    context = {}

    def get(self, request):
        self.context = {
            "grant_goals": models.GrantGoal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)


# Detail
class DetailGrantGoal(generic.View):
    template_name = "core/detail_gg.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "grant_goal": models.GrantGoal.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


### UPDATE
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/update_gg.html"
    model = models.GrantGoal
    form_class = forms.UpdateGrantGoalForm
    success_url = reverse_lazy("core:list_gg")

### DELETE
class DeleteGrantGoal(generic.DeleteView):
    template_name = "core/delete_gg.html"
    model = models.GrantGoal
    success_url = reverse_lazy("core:list_gg")



##### SUBGRANT GOAL CRUD #####
### CREATE
### RETRIEVE
## List
## Detail
### UPDATE
### DELETE


##### GOAL CRUD #####
### CREATE
### RETRIEVE
## List
## Detail
### UPDATE
### DELETE


##### ISSUE CRUD #####
### CREATE
### RETRIEVE
## List
## Detail
### UPDATE
### DELETE


##### AREA CRUD #####
### CREATE
### RETRIEVE
## List
## Detail
### UPDATE
### DELETE