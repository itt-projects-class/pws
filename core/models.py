from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Area(models.Model):
    area_name = models.CharField(max_length=64, default="Generic Area")
    timestamp = models.DateField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.area_name



CHOICES_STATE = (
    ("Done", "DN"),
    ("Doing", "DG"),
    ("Not Stared", "NS"),
)

class GrantGoal(models.Model):
    gg_name = models.CharField(max_length=128, default="Generic Grant Goal")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    days_durations = models.IntegerField(default=7)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    final_date = models.DateField(auto_now_add=False, auto_now=False, null=True, blank=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    progress = models.FloatField(default=0.0)
    state = models.CharField(max_length=16, choices=CHOICES_STATE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.gg_name
    

@receiver(post_save, sender=GrantGoal)
def final_date_grantgoal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_durations)
        instance.save()



class SubGrantGoal(models.Model):
    sgg_name = models.CharField(max_length=128, default="Generic Grant Goal")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grant_goal = models.ForeignKey(GrantGoal, on_delete=models.CASCADE)
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    days_durations = models.IntegerField(default=7)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    final_date = models.DateField(auto_now_add=False, auto_now=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    progress = models.FloatField(default=0.0)
    state = models.CharField(max_length=16, choices=CHOICES_STATE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.sgg_name


@receiver(post_save, sender=SubGrantGoal)
def final_date_subgrantgoal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_durations)
        instance.save()
    

class Goal(models.Model):
    goal_name = models.CharField(max_length=128, default="Generic Grant Goal")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subgrant_goal = models.ForeignKey(SubGrantGoal, on_delete=models.CASCADE)
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    days_durations = models.IntegerField(default=7)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    final_date = models.DateField(auto_now_add=False, auto_now=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    progress = models.FloatField(default=0.0)
    state = models.CharField(max_length=16, choices=CHOICES_STATE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.goal_name
    

@receiver(post_save, sender=Goal)
def final_date_goal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_durations)
        instance.save()


class Issue(models.Model):
    issue_name = models.CharField(max_length=128, default="Generic Grant Goal")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    days_durations = models.IntegerField(default=7)
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    final_date = models.DateField(auto_now_add=False, auto_now=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    progress = models.FloatField(default=0.0)
    state = models.CharField(max_length=16, choices=CHOICES_STATE)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.issue_name


@receiver(post_save, sender=Issue)
def final_date_issue(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_durations)
        instance.save()