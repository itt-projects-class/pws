from django import forms

from core import models


class CreateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.GrantGoal
        exclude = [
            "timestamp",
            "final_date",
            "status"
        ]
        widgets = {
            "gg_name": forms.TextInput({"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del GrantGoal"}),
            "user": forms.Select({"type": "select", "class": "form-select"}),
            "description": forms.Textarea({"type": "text", "class": "form-control", "rows": 3}),
            "days_durations": forms.NumberInput({"type": "number", "class": "form-control"}),
            "area": forms.Select({"type": "select", "class": "form-select"}),
            "progress": forms.NumberInput({"type": "number", "class": "form-control"}),
            "state": forms.Select({"type": "select", "class": "form-select"}),
            "slug": forms.TextInput({"type": "text", "class": "form-control", "placeholder": "Escribe el codigo del GrantGoal"}),
        }


class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.GrantGoal
        exclude = [
            "timestamp",
            "final_date",
           
        ]
        widgets = {
            "gg_name": forms.TextInput({"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del GrantGoal"}),
            "user": forms.Select({"type": "select", "class": "form-select"}),
            "description": forms.Textarea({"type": "text", "class": "form-control", "rows": 3}),
            "days_durations": forms.NumberInput({"type": "number", "class": "form-control"}),
            "area": forms.Select({"type": "select", "class": "form-select"}),
            "progress": forms.NumberInput({"type": "number", "class": "form-control"}),
            "state": forms.Select({"type": "select", "class": "form-select"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "slug": forms.TextInput({"type": "text", "class": "form-control", "placeholder": "Escribe el codigo del GrantGoal"}),
        }
        