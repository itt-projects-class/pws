from django.db import models

# Create your models here.

class Algo(models.Model):
    name = models.CharField(max_length=16)
    email = models.CharField(max_length=16)
    age = models.IntegerField()
    status = models.BooleanField()
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name