from django.apps import AppConfig


class ApiclientsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apiclients'
