from django.shortcuts import render
from django.views import generic
import requests

# Create your views here.


class ListGrantGoalClient(generic.View):
    url = "http://localhost:8000/api/v1/list/grantgoal/"
    context = {}
    response = None
    template_name = "apiclients/list_cl.html"

    def get(self, request):
        self.response = requests.get(url=self.url)

        self.context = {
            "response": self.response.json()
        }
        return render(request, self.template_name, self.context)



class DetailGrantGoalClient(generic.View):
    template_name = "apiclients/c_detail_gg.html"
    context = {}
    url = "http://localhost:8000/api/v1/detail/grantgoal/"
    response = None

    def get(self, request, pk):
        self.url += f"{pk}/"
        self.response = requests.get(self.url).json()
        self.context = {
            "grant_goal": self.response
        }
        return render(request, self.template_name, self.context)