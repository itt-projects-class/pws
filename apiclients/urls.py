from django.urls import path

from apiclients import views


app_name = "apiclient"


urlpatterns = [
    path('list/grantgoal/client/', views.ListGrantGoalClient.as_view(), name="c_list_gg"),
    path('detail/grantgoal/client/<int:pk>/', views.DetailGrantGoalClient.as_view(), name="c_detail_gg"),
]
