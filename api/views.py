from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics

from core.models import GrantGoal
from .serializers import ListGrantGoalSerializer, DetailGrantGoalSerializer, CreateGrantGoalSerializer, UpdateGrantGoalSerializer, DeleteGrantGoalSerializer

# Create your views here.


class ListGrantGoalAPIView(APIView):
    def get(self, request):
        ggoals = GrantGoal.objects.filter(status=True)
        data = ListGrantGoalSerializer(ggoals, many=True).data
        return Response(data)


class DetailGrantGoalAPIView(APIView):
    def get(self, request, pk):
        ggoal = GrantGoal.objects.get(pk=pk)
        data = DetailGrantGoalSerializer(ggoal, many=False).data
        return Response(data)
    

class CreateGrantGoalAPIView(generics.CreateAPIView):
    model = GrantGoal
    serializer_class = CreateGrantGoalSerializer


class UpdateGrantGoalAPIView(generics.UpdateAPIView):
    queryset = GrantGoal.objects.all()
    serializer_class = UpdateGrantGoalSerializer


class DeleteGrantGoalAPIView(generics.DestroyAPIView):
    queryset = GrantGoal.objects.all()
    serializer_class = DeleteGrantGoalSerializer