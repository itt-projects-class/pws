from rest_framework import serializers

from core.models import GrantGoal

class ListGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        # fields = "__all__"
        exclude = ["final_date", "days_durations", "state", "user", "progress", "area"]


class DetailGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"



class CreateGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"




class UpdateGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"


class DeleteGrantGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"